<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ForecastController;
use App\Http\Controllers\LocationsController;
use App\Http\Controllers\AddLocationController;
use App\Http\Controllers\ChangeLocationController;
use App\Http\Controllers\DeleteLocationController;
use App\Http\Controllers\DefaultLocationController;
use App\Http\Controllers\UpdateUnitsImperialController;
use App\Http\Controllers\UpdateUnitsMetricController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ForecastController::class)->name('home');

Route::get('/forecast', ForecastController::class)->name('forecast');

Route::get('/imperial', UpdateUnitsImperialController::class)->name('imperial');

Route::get('/metric', UpdateUnitsMetricController::class)->name('metric');

Route::get('/locations', LocationsController::class)->name('locations');

Route::get('/change_location', ChangeLocationController::class)->name('change_locations');

Route::get('/search', SearchController::class)->name('search');

Route::post('/add_location', AddLocationController::class)->name('add_location')->middleware('auth');

Route::get('/add_location', function () {
  return redirect()->route('locations');
});

Route::post('/default_location', DefaultLocationController::class)->name('default_location')->middleware('auth');

Route::get('/default_location', function () {
  return redirect()->route('locations');
});

Route::post('/delete_location', DeleteLocationController::class)->name('delete_location')->middleware('auth');

Route::get('/delete_location', function () {
  return redirect()->route('locations');
});

require __DIR__ . '/auth.php';
