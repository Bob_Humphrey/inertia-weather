<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
  <meta name="description"
    content="Seven day weather forecast for any location, based on the OpenWeather API. Built with Laravel, Inertia.js, Svelte and Tailwind CSS." />
  <title>Weather Forecast</title>
  <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
  <script src="{{ mix('/js/app.js') }}" defer></script>
</head>

<body class="bg-gray-100 w-full font-nunito_regular px-6 xl:px-20 pt-2 pb-4">
  {{-- @include('layouts.screen-width') --}}
  {{-- @include('flash-message') --}}
  @inertia
</body>

</html>
