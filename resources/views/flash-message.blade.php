@if ($message = Session::get('success'))
  <div class="w-full bg-indigo-700 text-white font-nunito_bold text-center px-6 py-2 mb-2">
    {{ $message }}
  </div>
@endif

@if ($message = Session::get('error'))
  <div class="w-full bg-red-700 text-white font-nunito_bold text-center px-6 py-2 mb-2">
    {{ $message }}
  </div>
@endif
