<div class="flex text-xs justify-center w-full text-gray-700 mb-2 space-x-4 lg:space-x-10">
  <a class="hidden lg:block" href="/">
    {{ $homeLocation }}
  </a>
  <a class="block lg:hidden" href="/">
    Home
  </a>
  <a class="hidden lg:block" href="/imperial">
    Fahrenheit
  </a>
  <a class="block lg:hidden" href="/imperial">
    F
  </a>
  <a class="hidden lg:block" href="/metric">
    Celsius
  </a>
  <a class="block lg:hidden" href="/metric">
    C
  </a>
  <a class="hidden lg:block" href="/locations">
    Change Location
  </a>
  <a class="block lg:hidden" href="/locations">
    Change
  </a>
  @guest
    <a class="" href="/register">
      Signup
    </a>
    <a class="" href="/login">
      Login
    </a>
  @endguest
  @auth
    <a class="" href="/logout">
      Logout
    </a>
  @endauth
</div>
