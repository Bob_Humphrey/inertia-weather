<?php
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('darksky/{latitude}/{longitude}/{units}', function ($latitude, $longitude, $units) {

  $latitude = str_replace("X", ".", $latitude);
  $longitude = str_replace("X", ".", $longitude);

  $query_string = 'https://api.darksky.net/forecast/'
    . 'f61e4dfb6850a960d2152aa42b7e0d5a/'
    . $latitude
    . ','
    . $longitude
    . '?exclude=minutely,flags&units='
    . $units
    . '&extend=hourly';

  $client = new Client();
  $data = json_decode((string)$client->get($query_string)->getBody());
  return response()->json($data)
    ->header('Access-Control-Allow-Origin', 'https://weather.bob-humphrey.com')
    ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
});

$router->get('opencage/{location}', function ($location) {

  $query_string = 'https://api.opencagedata.com/geocode/v1/json?q=' . $location . '&key=89af0aec75434a53ab4b37fbc7011d89&language=en&limit=7';

  $client = new Client();
  $data = json_decode((string)$client->get($query_string)->getBody());
  return response()->json($data)
    ->header('Access-Control-Allow-Origin', 'https://weather.bob-humphrey.com')
    ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
});
