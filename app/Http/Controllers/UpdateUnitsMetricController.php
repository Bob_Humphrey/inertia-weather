<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateUnitsMetricController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    session(['units' => 'metric']);
    if (Auth::check()) {
      $id = Auth::user()->id;
      $user = User::find($id);
      $user->units = 'metric';
      $user->save();
    }
    return redirect()->route('forecast');
  }
}
