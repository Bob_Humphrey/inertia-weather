<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Actions\GetLocationsAction;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class DeleteLocationController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $statusMessage = null;

    if (Auth::check()) {
      $location = Location::find($request->id);
      $locationName = $location->name;
      $location->delete();
      $statusMessage = "$locationName has been removed from the list of saved locations.";
    }

    $locations = GetLocationsAction::execute();

    return Inertia::render('Locations', [
      'locations' => $locations,
      'findLocations' => [],
      'statusMessage' => $statusMessage
    ]);
  }
}
