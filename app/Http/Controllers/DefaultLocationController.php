<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Actions\GetLocationsAction;
use Illuminate\Support\Facades\Auth;

class DefaultLocationController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $statusMessage = null;

    if (Auth::check()) {
      $id = Auth::user()->id;
      $location = Location::where('user', $id)->where('default', TRUE)
        ->first();
      if ($location) {
        $location->default = FALSE;
        $location->save();
      }
      $location = Location::find($request->id);
      $location->default = TRUE;
      $location->save();
      $statusMessage = "The default location is now $location->name.";
    }

    $locations = GetLocationsAction::execute();

    return Inertia::render('Locations', [
      'locations' => $locations,
      'findLocations' => [],
      'statusMessage' => $statusMessage
    ]);
  }
}
