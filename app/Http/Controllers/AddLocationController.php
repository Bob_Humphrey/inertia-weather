<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Actions\GetLocationsAction;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class AddLocationController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $statusMessage = null;

    if (Auth::check()) {
      $location = new Location;
      $location->user = Auth::user()->id;
      $location->name = $request->name;
      $location->lat = $request->latitude;
      $location->long = $request->long;
      $location->default = FALSE;
      $location->save();

      $statusMessage = "$request->name has been added to the list of saved locations.";
    }

    $locations = GetLocationsAction::execute();

    return Inertia::render('Locations', [
      'locations' => $locations,
      'findLocations' => [],
      'statusMessage' => $statusMessage
    ]);
  }
}
