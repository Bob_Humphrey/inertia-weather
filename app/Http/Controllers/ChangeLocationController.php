<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChangeLocationController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $location = [];
    $location['default'] = $request->default;
    $location['name'] = $request->name;
    $location['lat'] = $request->latitude;
    $location['long'] = $request->long;
    session(['location' => $location]);
    return redirect()->route('forecast');
  }
}
