<?php

namespace App\Http\Controllers;

use Config;
use Inertia\Inertia;
use GuzzleHttp\Client;
use App\Models\ApiError;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;

class ForecastController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $openWeatherKey = config('keys.open_weather_key');
    $units = session('units');
    $location = session('location');
    // $location['long'] = 9999999;

    $latitude = $location['lat'];
    $longitude = $location['long'];
    $location = $location['name'];

    $query_string = "https://api.openweathermap.org/data/2.5/onecall?lat=$latitude&lon=$longitude&units=$units&appid=$openWeatherKey";

    $client = new Client();

    $response = $client->get($query_string, ['http_errors' => false]);
    $data = json_decode((string)$response->getBody());
    $status = $response->getStatusCode();

    if ($status === 200) {
      // Good response from API.
    } else {
      // Handle error. 
      $reason = $response->getReasonPhrase();
      $errorBody = json_decode((string) $response->getBody());
      $errorMessage = $errorBody->message;
      $apiError = new ApiError();
      $userId = 0;
      if (Auth::check()) {
        $user = Auth::user();
        $userId = $user->id;
      }
      $apiError->api = 'OpenWeather';
      $apiError->user = $userId;
      $apiError->status = $status;
      $apiError->message = $errorMessage;
      $apiError->save();

      return Inertia::render(
        'SevenDayForecastError',
        []
      );
    }

    // Show the data returned from the API

    // $response = $client->get($query_string)->getBody();
    // return response()->json($data)
    //   ->header('Access-Control-Allow-Origin', '*')
    //   ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    // Next 3 days

    $next3days = [];
    $next3daysAbbrev = [];
    $day = new \stdClass;
    $id = Carbon::createFromTimestamp($data->daily[0]->dt, $data->timezone)->format('D');
    $day->sunrise = $data->daily[0]->sunrise;
    $day->sunset = $data->daily[0]->sunset;
    $next3days[$id] = $day;
    $next3daysAbbrev[0] = $id;
    $day = new \stdClass;
    $id = Carbon::createFromTimestamp($data->daily[1]->dt, $data->timezone)->format('D');
    $day->sunrise = $data->daily[1]->sunrise;
    $day->sunset = $data->daily[1]->sunset;
    $next3days[$id] = $day;
    $next3daysAbbrev[1] = $id;
    $day = new \stdClass;
    $id = Carbon::createFromTimestamp($data->daily[2]->dt, $data->timezone)->format('D');
    $day->sunrise = $data->daily[2]->sunrise;
    $day->sunset = $data->daily[2]->sunset;
    $next3days[$id] = $day;
    $next3daysAbbrev[2] = $id;

    // Current

    $current = new \stdClass;
    $current->dt = Carbon::createFromTimestamp($data->current->dt, $data->timezone)->format('l F j, Y g:i A');
    $current->temp = intval($data->current->temp);
    $current->feelsLike = intval($data->current->feels_like);
    $current->weatherId = $data->current->weather[0]->id;
    $description = '';
    $x = 0;
    foreach ($data->current->weather as $weather) {
      if ($x > 0) $description .= ', ';
      $description .= $weather->description;
      $x++;
    }
    $current->description  = $description;
    $current->humidity = $data->current->humidity;
    $current->dewPoint = intval($data->current->dew_point);
    $current->windSpeed = intval($data->current->wind_speed);
    $windDegrees = $data->current->wind_deg;
    $current->dayNight = $data->current->dt > $data->current->sunrise && $data->current->dt < $data->current->sunset ? 'day' : 'night';
    $current->windDirection = '';
    if ($windDegrees >= 0 && $windDegrees < 11.25) {
      $current->windDirection = 'N';
    } else if ($windDegrees >= 11.25 && $windDegrees < 33.75) {
      $current->windDirection = 'NNE';
    } else if ($windDegrees >= 33.75 && $windDegrees < 56.25) {
      $current->windDirection = 'NE';
    } else if ($windDegrees >= 56.25 && $windDegrees < 78.75) {
      $current->windDirection = 'ENE';
    } else if ($windDegrees >= 78.75 && $windDegrees < 101.25) {
      $current->windDirection = 'E';
    } else if ($windDegrees >= 101.25 && $windDegrees < 123.75) {
      $current->windDirection = 'ESE';
    } else if ($windDegrees >= 123.75 && $windDegrees < 146.25) {
      $current->windDirection = 'SE';
    } else if ($windDegrees >= 146.25 && $windDegrees < 168.75) {
      $current->windDirection = 'SSE';
    } else if ($windDegrees >= 168.75 && $windDegrees < 191.25) {
      $current->windDirection = 'S';
    } else if ($windDegrees >= 191.25 && $windDegrees < 213.75) {
      $current->windDirection = 'SSW';
    } else if ($windDegrees >= 213.75 && $windDegrees < 236.25) {
      $current->windDirection = 'SW';
    } else if ($windDegrees >= 236.25 && $windDegrees < 258.75) {
      $current->windDirection = 'WSW';
    } else if ($windDegrees >= 258.75 && $windDegrees < 281.25) {
      $current->windDirection = 'W';
    } else if ($windDegrees >= 281.25 && $windDegrees < 303.75) {
      $current->windDirection = 'WNW';
    } else if ($windDegrees >= 303.75 && $windDegrees < 326.25) {
      $current->windDirection = 'NW';
    } else if ($windDegrees >= 326.25 && $windDegrees < 348.75) {
      $current->windDirection = 'NNW';
    } else if ($windDegrees >= 348.75) {
      $current->windDirection = 'N';
    }

    // Minutely

    $next60minutes = [];
    if (property_exists($data, 'minutely')) {
      for ($i = 0; $i < 60; $i++) {
        $next60minutes[$i] = intval(ceil($data->minutely[$i]->precipitation));
      }
    }

    // Hourly

    $hourly = [];
    for ($i = 0; $i < 48; $i++) {
      $hour = new \stdClass;
      $hour->temp = intval($data->hourly[$i]->temp);
      $day = Carbon::createFromTimestamp($data->hourly[$i]->dt, $data->timezone)->format('D');
      $hour->day = $day;
      $hour->hour = Carbon::createFromTimestamp($data->hourly[$i]->dt, $data->timezone)->format('g a');
      if ($data->hourly[$i]->dt > $next3days[$day]->sunrise && $data->hourly[$i]->dt < $next3days[$day]->sunset) {
        $hour->dayNight = 'day';
      } else {
        $hour->dayNight = 'night';
      }
      $hour->weatherId = $data->hourly[$i]->weather[0]->id;
      $hourly[$i] = $hour;
    }

    $day = new \stdClass;
    $day->temp = 'none';
    $day->day = '';
    $day->hour = '';
    $day->dayNight = 'none';
    $day->weatherId = 0;

    $next48hours = [];
    for ($i = 0; $i < 3; $i++) {
      $next48hours[$next3daysAbbrev[$i]]['12 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['1 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['2 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['3 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['4 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['5 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['6 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['7 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['8 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['9 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['10 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['11 am'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['12 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['1 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['2 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['3 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['4 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['5 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['6 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['7 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['8 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['9 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['10 pm'] = $day;
      $next48hours[$next3daysAbbrev[$i]]['11 pm'] = $day;
    }

    for ($i = 0; $i < 48; $i++) {
      $day = $hourly[$i]->day;
      $hour = $hourly[$i]->hour;
      $next48hours[$day][$hour] = $hourly[$i];
    }

    // Daily

    $next7days = [];
    for ($i = 0; $i < 7; $i++) {
      $day = new \stdClass;
      $day->day =
        Carbon::createFromTimestamp($data->daily[$i]->dt, $data->timezone)->format('D');
      if ($i === 0) $day->day = 'Today';
      $day->weatherId = $data->daily[$i]->weather[0]->id;
      $day->highTemp = intval(round($data->daily[$i]->temp->max));
      $day->lowTemp = intval(round($data->daily[$i]->temp->min));
      $day->precipitation = intval($data->daily[$i]->pop * 100);
      $day->dewPoint = intval(round($data->daily[$i]->dew_point));
      $day->sunrise =
        Carbon::createFromTimestamp($data->daily[$i]->sunrise, $data->timezone)->format('g:i');
      $day->sunset =
        Carbon::createFromTimestamp($data->daily[$i]->sunset, $data->timezone)->format('g:i');
      $day->fullMoon = $data->daily[$i]->moon_phase === .5 ? TRUE : FALSE;
      $day->dayNight = 'day';
      $next7days[$i] = $day;
    }

    // Alerts

    $alerts = [];
    if (property_exists($data, 'alerts')) {
      foreach ($data->alerts as $item) {
        $alert = new \stdClass;
        $alert->event = $item->event;
        $alert->description = $item->description;
        $alerts[] = $alert;
      }
    }

    // Log::info($next7days);

    // Return page component

    return Inertia::render('SevenDayForecast', [
      'location' => $location,
      'units' => $units,
      'current' => $current,
      'next3daysAbbrev' => $next3daysAbbrev,
      'next60minutes' => $next60minutes,
      'next48hours' => $next48hours,
      'next7days' => $next7days,
      'alerts' => $alerts,
    ]);
  }
}
