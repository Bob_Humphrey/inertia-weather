<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateUnitsImperialController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    session(['units' => 'imperial']);
    if (Auth::check()) {
      $id = Auth::user()->id;
      $user = User::find($id);
      $user->units = 'imperial';
      $user->save();
    }
    return redirect()->route('forecast');
  }
}
