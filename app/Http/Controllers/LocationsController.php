<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Actions\GetLocationsAction;
use Illuminate\Support\Facades\Auth;

class LocationsController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $locations = GetLocationsAction::execute();

    return Inertia::render('Locations', [
      'locations' => $locations,
      'findLocations' => [],
      'statusMessage' => null,

    ]);
  }
}
