<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use GuzzleHttp\Client;
use App\Models\ApiError;
use Illuminate\Http\Request;
use App\Actions\GetLocationsAction;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {

    // Search for locations request - empty search field

    $locations = GetLocationsAction::execute();
    $positionstackKey = config('keys.positionstack_key');
    $findLocations = [];
    $findLocation = $request->location;

    if (trim($findLocation) === '') {
      return Inertia::render('Locations', [
        'locations' => $locations,
        'findLocations' => $findLocations,
        'statusMessage' => null
      ]);
    }

    $query_string = "http://api.positionstack.com/v1/forward?access_key=$positionstackKey&query=$findLocation&limit=15&output=json";
    $client = new Client();
    $response = $client->get($query_string, ['http_errors' => false]);
    $data = json_decode((string)$response->getBody());
    // $data = json_decode((string)$client->get($query_string)->getBody());
    $status = $response->getStatusCode();

    // Show the data returned from the API

    // $response = $client->get($query_string)->getBody();
    // return response()->json($data)
    //   ->header('Access-Control-Allow-Origin', '*')
    //   ->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    if ($status === 200) {
      // Good response from API.
    } else {
      // Handle error. 
      $reason = $response->getReasonPhrase();
      $errorBody = json_decode((string) $response->getBody());
      // dd($errorBody);
      $statusMessage = $errorBody->error->context->query->message;
      $apiError = new ApiError();
      $userId = 0;
      if (Auth::check()) {
        $user = Auth::user();
        $userId = $user->id;
      }
      $apiError->api = 'PositionStack';
      $apiError->user = $userId;
      $apiError->status = $status;
      $apiError->message = $statusMessage;
      $apiError->save();

      if ($statusMessage === "query must have at 3 character's") {
        $statusMessage = "At least 3 characters required for a search.";
      }

      return Inertia::render('Locations', [
        'locations' => $locations,
        'findLocations' => $findLocations,
        'statusMessage' => $statusMessage
      ]);
    }

    $statusMessage = null;
    $data = $data->data;
    if (count($data) === 0) {
      $statusMessage = "No locations could be found for $findLocation.";
    }

    foreach ($data as $result) {
      $locationChoice = [];
      $name = str_replace(', USA', '', $result->label);
      $name = str_replace(', Canada', '', $name);
      $locationChoice['name'] = $name;
      $locationChoice['lat'] = $result->latitude;
      $locationChoice['long'] = $result->longitude;
      $findLocations[] = $locationChoice;
    }

    return Inertia::render('Locations', [
      'locations' => $locations,
      'findLocations' => $findLocations,
      'statusMessage' => $statusMessage
    ]);
  }
}
