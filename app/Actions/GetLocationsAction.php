<?php

namespace App\Actions;

use App\Models\Location;
use Illuminate\Support\Facades\Auth;

class GetLocationsAction
{
  public static function execute(): array
  {

    $locations = [];

    if (Auth::check()) {
      $id = Auth::user()->id;
      $locations = Location::where('user', $id)->orderBy('name')->get()->toArray();
    }

    return $locations;
  }
}
