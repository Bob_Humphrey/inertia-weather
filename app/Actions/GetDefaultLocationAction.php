<?php

namespace App\Actions;

use App\Models\Location;
use Illuminate\Support\Facades\Auth;

class GetDefaultLocationAction
{
  public static function execute($default): array
  {
    // if ($default === 'Santa Ana') {
    $defaultLocation = [];
    $defaultLocation['default'] = TRUE;
    $defaultLocation['name'] = 'Santa Ana CA';
    $defaultLocation['lat'] = 33.749741;
    $defaultLocation['long'] = -117.869781;
    return $defaultLocation;
    // } else {
    //   $defaultLocation = [];
    //   $defaultLocation['default'] = TRUE;
    //   $defaultLocation['name'] = 'New York, NY';
    //   $defaultLocation['lat'] = 40.712772;
    //   $defaultLocation['long'] = -74.006058;
    // }

    if (Auth::check()) {
      $id = Auth::user()->id;
      $location = Location::where('user', $id)->where('default', TRUE)
        ->first();
      if ($location) {
        return $location->toArray();
      }
    }

    return $defaultLocation;
  }
}
