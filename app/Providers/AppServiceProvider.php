<?php

namespace App\Providers;

use App\Actions\GetDefaultLocationAction;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    session(['units' => 'imperial']);
    $location = GetDefaultLocationAction::execute('');
    session(['location' => $location]);
  }
}
