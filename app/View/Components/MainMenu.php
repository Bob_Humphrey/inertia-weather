<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MainMenu extends Component
{
  /**
   * Create a new component instance.
   *
   * @return void
   */

  public $homeLocation;

  public function __construct()
  {
    $location = session('location');
    $this->homeLocation = $location['name'];
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|\Closure|string
   */
  public function render()
  {

    return view('components.main-menu');
  }
}
