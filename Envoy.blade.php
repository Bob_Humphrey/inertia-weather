@servers(['main' => ['bobdylan@165.227.194.76']])

@setup
$app = 'openweather.bob-humphrey.com';
$dir = '/var/www/' . $app;
$rep = 'git@gitlab.com:Bob_Humphrey/inertia-weather.git';
@endsetup

@task('list', ['on'=> 'main'])
cd {{ $dir }}
ls
@endtask

@task('clear', ['on'=> 'main'])
cd /var/www
echo "{{ $password }}" | sudo -S rm -rf {{ $dir . '/*' }}
echo "{{ $password }}" | sudo -S rm -rf {{ $dir . '/.*' }}
@endtask

@task('deploy-new', ['on'=> 'main'])
cd /var/www
echo "{{ $password }}" | sudo -S chmod 777 {{ $app }}
cd /var/www
git clone {{ $rep }} {{ $app }}
cd {{ $dir }}
composer install --no-dev --no-interaction --no-plugins --no-scripts --no-progress --optimize-autoloader
echo "{{ $password }}" | sudo -S chmod 775 {{ $dir . '/storage' }}
echo "{{ $password }}" | sudo find {{ $dir . '/storage' }} -type d -exec chmod 775 {} \;
echo "{{ $password }}" | sudo find {{ $dir . '/storage' }} -type f -exec chmod 664 {} \;
echo "{{ $password }}" | sudo -S chown -R www-data:bobdylan {{ $dir }}
echo "{{ $password }}" | sudo -S chmod 775 {{ $dir }}
echo "{{ $password }}" | sudo touch {{ $dir . '/storage/logs/laravel.log' }}
echo "{{ $password }}" | sudo -S chown www-data:bobdylan {{ $dir . '/storage/logs/laravel.log' }}
echo "{{ $password }}" | sudo -S chmod 664 {{ $dir . '/storage/logs/laravel.log' }}
php artisan cache:clear
php artisan config:cache
@endtask

@task('deploy-new-part2', ['on'=> 'main'])
cd {{ $dir }}
echo "{{ $password }}" | sudo -S chmod 660 {{ $dir . '/.env' }}
composer dump-autoload -o
php artisan config:cache
@endtask

@task('deploy-update', ['on'=> 'main'])
cd {{ $dir }}
php artisan down
git fetch
git checkout main
git pull
composer install --no-dev --no-interaction --no-plugins --no-scripts --no-progress --optimize-autoloader
composer dump-autoload -o
# php artisan migrate --force
php artisan cache:clear
php artisan config:cache
# RUN THE COMMANDS TO READ THE STORAGE FILES AND UPDATE THE DATABASE
# php artisan app:director
php artisan up
@endtask

@task('deploy-update-codeonly', ['on'=> 'main'])
cd {{ $dir }}
php artisan down
git fetch
git checkout main
git pull
composer install --no-dev --no-interaction --no-plugins --no-scripts --no-progress --optimize-autoloader
# php artisan migrate --force
php artisan cache:clear
php artisan config:cache
php artisan up
@endtask
