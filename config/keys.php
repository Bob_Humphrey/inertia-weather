<?php
return [
  'open_weather_key' => env('OPEN_WEATHER_KEY'),
  'positionstack_key' => env('POSITIONSTACK_KEY')
];
